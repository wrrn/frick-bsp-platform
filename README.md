# Frick BSP

## 1. Install the repo utility

```bash
$ mkdir ~/bin
$ curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo
```

## 4. Download BSP Source

```bash
$ mkdir frick-bsp
$ cd frick-bsp
$ repo init -u https://bitbucket.org/prmunty/frick-bsp-platform -b krogoth
$ repo sync
```

## 3. Run setup-environment

```bash
$ MACHINE=qemu source setup-environment build
```

## 4. Add layers to the conf/bblayers.conf
conf/bblayers.conf is located at ```/path/to/frick-bsp/build/conf/bblayers.conf```.

Add the following to the ```BBLAYERS``` variable in the bblayers.conf file:

```
  \
  ${BSPDIR}/sources/meta-openembedded/meta-gnome \
  ${BSPDIR}/sources/meta-openembedded/meta-efl \
  \
  ${BSPDIR}/sources/openembedded-core/meta \    
  ${BSPDIR}/sources/openembedded-core/meta-skeleton \    
  \
  ${BSPDIR}/sources/meta-frick \
  ${BSPDIR}/sources/meta-browser \  
  ${BSPDIR}/sources/meta-golang \  
  ${BSPDIR}/sources/meta-qt4 \  
  \
  ${BSPDIR}/sources/meta-openembedded/meta-oe \
  ${BSPDIR}/sources/meta-openembedded/meta-perl \
  ${BSPDIR}/sources/meta-openembedded/meta-python \
  ${BSPDIR}/sources/meta-openembedded/meta-networking \
  ${BSPDIR}/sources/meta-security \
```

# 5. Build the image

```bash
bitbake frick-image
```


